#!/usr/bin/python3
import zipfile
import sys
zf = zipfile.ZipFile(sys.argv[1],'r')
print (f"======== Filelisting for {sys.argv[1]} ========")
for f in zf.filelist:
    d=f.date_time
    print(f"{f.filename} ({f.file_size}) {d[0]}/{d[1]:02}/{d[2]:02} {d[3]:02}:{d[4]:02}:{d[5]:02}")