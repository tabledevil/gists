disablefile=$(realpath ~/.notermlogs)
if [[ -f "${disablefile}" ]] ; then
    echo "${disablefile} exists > Terminal - Logging is disabled"
else
    if [[ -z "${termlogpid}" ]] ; then
        export termlogpid="$(echo $$)"
        export termlogsdir="${HOME}/termlogs"
        export termlogfilename="${termlogsdir}/$(date +%Y-%m-%d_%H:%M:%S)_${termlogpid}"
        if ! [[ -d "${termlogsdir}" ]] ; then
            echo "Terminal log folder does not exist. ${termlogsdir} will be created..."
            mkdir -v -p "${termlogsdir}"
        fi
        #Cleanup old files
        echo "Removin Terminal Logs older than 30 days:"
        find "${termlogsdir}" -type f -mtime +30 
        find "${termlogsdir}" -type f -mtime +30 -delete

        #prepare outputfiles with proper permissions
        touch "${termlogfilename}.termlog"
        chmod 600 "${termlogfilename}.termlog"
        touch "${termlogfilename}.termtime"
        chmod 600 "${termlogfilename}.termtime"
        #start logged session
        script -t"${termlogfilename}.termtime" "${termlogfilename}.termlog"
        #clean up after exiting
        gzip "${termlogfilename}.termlog"
        if [[ -f "/tmp/disablescreenrecord_${termlogpid}" ]] ; then
            echo "Recording stopped for this session"
        else
            exit
        fi
    else
        alias stop_termlog="touch /tmp/disablescreenrecord_${termlogpid} ; exit"
    fi
fi